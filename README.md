## create a new dotnet console app w/ RabbitMQ Client
```bash
# make sure that the dir does not have a folder called APP_NAME
$ dotnet new console --name {APP_NAME}
$ cd {APP_NAME}
$ dotnet add package RabbitMQ.Client
$ dotnet restore
```

---

## RabbitMQ docker image
### setup
see `00-docker-setup` for more details

### run the container
```bash
$ docker ps -a
$ docker run {CONTAINER ID}
```
