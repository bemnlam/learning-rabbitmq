# Dockerfile and Docker Compose

## build docker image from RabbitMQ image

### using Docker Compose
```bash
$ docker-compose build
$ docker-compose up
```
RabbitMQ is then running at port 5672. 
Web admin UI is hosted at [localhost:15672](http://localhost:15672)
> username: `guest`;  
> password: `guest`

### making dockerfile
(`Dockerfile` is base on [this](https://github.com/docker-library/rabbitmq/blob/4b2b11c59ee65c2a09616b163d4572559a86bb7b/3.7/debian/management/Dockerfile) dockerfile)

```bash
# building a dockerimage tagged "rabbit-mgt"
$ docker build --tag rabbitmq-mgt . --no-cache
```

you will see the following message if build is successful

```bash
# some other messages...
Successfully built 5f358902b958
Successfully tagged rabbitmq-mgt:latest
```

run docker container
```
$ docker run rabbitmq-mgt:latest
```