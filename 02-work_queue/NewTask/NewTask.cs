﻿using System;
using RabbitMQ.Client;
using System.Text;

namespace NewTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var factory = new ConnectionFactory() {
                HostName = "localhost"
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(
                        queue: "task_queue",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );

                    string timestamp = DateTime.Now.Ticks.ToString();
                    string message = string.Format("{0} | {1}", timestamp, GetMessage(args));

                    var body = Encoding.UTF8.GetBytes(message); // TODO: encode content brefore send

                    var properties = channel.CreateBasicProperties();
                    properties.Persistent = true;

                    channel.BasicPublish(
                        exchange: "",
                        routingKey: "task_queue",
                        basicProperties: properties,
                        body: body
                    );

                    Console.WriteLine(" [x] Sent {0}", message);
                }

                // Console.WriteLine(" Press [enter] to exit.");
                // Console.ReadLine();
            }
        }

        private static string GetMessage(string[] args)
        {
            return ((args.Length > 0) ? string.Join(" ", args) : "Hello World");
        }
    }
}
